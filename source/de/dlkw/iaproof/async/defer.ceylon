import ceylon.promise {
    Promise,
    Deferred
}

shared variable Anything(Anything()) defer = (Anything() p)
{
    throw AssertionError("A runtime specific implementation must be assigned.");
};

shared Promise<Result> doDefer<Result>(Result() calculateResult)
{
    value deferred = Deferred<Result>();
    defer(() => deferred.fulfill(calculateResult()));
    return deferred.promise;
}