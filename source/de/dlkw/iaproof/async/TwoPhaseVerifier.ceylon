import de.dlkw.iaproof.async.io {
    A=Codec,
    Communication
}
import de.dlkw.iaproof.sync {
    ProgressIndicator
}
import ceylon.promise {
    Promise,
    Deferred
}

shared abstract class TwoPhaseVerifier<Phase1Result, Challenge, Phase2Result>(
        codecPhase1,
        codecChallenge,
        codecPhase2)

        satisfies Verifier
        given Phase1Result satisfies Object
        given Challenge satisfies Object
        given Phase2Result satisfies Object
{
    A<Phase1Result> codecPhase1;
    A<Challenge> codecChallenge;
    A<Phase2Result> codecPhase2;

    shared actual Promise<Boolean> verify(Integer repeats, Communication proverIO, ProgressIndicator? progressIndicator)
    {
        return proverIO.writeInt32(repeats)
            .flatMap((ignored)
        {
            log.debug("v wrote repeats: ``repeats``");
            return proverIO.flush();
        })
            .flatMap((ignored)
        {
            if (exists progressIndicator) {
                progressIndicator.startSub(repeats, "verifying phases");
            }

            value promise = verifyRemainingPhases(repeats, proverIO, progressIndicator);

            if (exists progressIndicator) {
                progressIndicator.endSub();
            }

            return promise;
        });
    }

    Promise<Boolean> verifyRemainingPhases(Integer remainingRepeats, Communication proverIO, ProgressIndicator? progressIndicator)
    {
        assert (remainingRepeats > 0);

        log.debug("v still to go: ``remainingRepeats``");

        Promise<Phase1Result> commitmentPromise = codecPhase1.decode(proverIO);

        return commitmentPromise.flatMap((commitment)
        {
            return verifyPhase1(commitment);
        })
        .flatMap((passed)
        {
            if (!passed) {
                value deferred = Deferred<Boolean>();
                deferred.fulfill(false);
                return deferred.promise;
            }
            else {
                Challenge challenge = createChallenge();
                Promise<Phase2Result> receivedPhase2Promise = codecChallenge.encodeAndWrite(challenge, proverIO)
                .flatMap((ignored)
                {
                    log.debug("v wrote ``challenge``");
                    return proverIO.flush();
                })
                .flatMap((ignored)
                {
                    log.debug("v flushed");
                    return codecPhase2.decode(proverIO);
                });
                value x= receivedPhase2Promise.and(commitmentPromise);
                Promise<Boolean> y = x.flatMap((commitment, response)
                {
                    log.debug("v verify phase2");
                    return verifyPhase2(response, commitment, challenge);
                });
                return y.flatMap((passedPhase2)
                {
                    if (!passedPhase2) {
                        value deferred = Deferred<Boolean>();
                        deferred.fulfill(false);
                        return deferred.promise;
                    }
                    else {
                        if (exists progressIndicator) {
                            progressIndicator.advance();
                        }

                        if (remainingRepeats == 1) {
                            value deferred = Deferred<Boolean>();
                            deferred.fulfill(true);
                            return deferred.promise;
                        }
                        else {
                            return verifyRemainingPhases(remainingRepeats - 1, proverIO, progressIndicator);
                        }
                    }
                });
            }
        });
    }

    shared formal Promise<Boolean> verifyPhase1(Phase1Result commitment);
    shared formal Challenge createChallenge();
    shared formal Promise<Boolean> verifyPhase2(Phase2Result response, Phase1Result commitment, Challenge challenge);
}
