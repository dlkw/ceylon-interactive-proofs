import ceylon.promise {
    Promise,
    Deferred
}

shared interface Codec<Type>
{
    shared formal Promise<Anything> encodeAndWrite(Type val, OutputHelper communication);
    shared formal Promise<Type> decode(InputHelper communication);
}

shared object booleanCodec satisfies Codec<Boolean>
{
    shared actual Promise<Boolean> decode(InputHelper from)
    {
        return from.readByte()
            .flatMap((receivedVal)
        {
            value deferred = Deferred<Boolean>();
            if (exists receivedVal) {
                deferred.fulfill(!receivedVal.zero);
            }
            else {
                deferred.reject(EndOfData());
            }
            return deferred.promise;
        });
    }

    shared actual Promise<Anything> encodeAndWrite(Boolean data, OutputHelper to)
    {
        return to.writeByte(data then 1.byte else 0.byte);
    }
}

shared class EndOfData() extends Exception(){}
