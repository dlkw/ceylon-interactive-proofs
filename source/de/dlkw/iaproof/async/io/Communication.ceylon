import ceylon.buffer {
    ByteBuffer
}
import ceylon.buffer.charset {
    utf8
}
import ceylon.promise {
    Deferred,
    Promise
}

shared Promise<T> emptyPromise<T>(T val)
{
    value deferred = Deferred<T>();
    deferred.fulfill(val);
    return deferred.promise;
}

shared interface InputHelper
{
    shared formal Promise<Byte?> readByte();
    shared formal Promise<Integer> readInt32();
    shared formal Promise<[Byte*]> readBytes();
    shared formal Promise<String> readString();
}

shared interface OutputHelper
{
    shared formal Promise<Anything> writeByte(Byte val);
    shared formal Promise<Anything> writeInt32(Integer val);
    shared formal Promise<Anything> writeBytes({Byte*} bytes);
    shared formal Promise<Anything> writeString(String val);

    shared formal Promise<Anything> flush();
}

shared interface Communication
        satisfies InputHelper & OutputHelper
{}


shared abstract class CommunicationCore()
        satisfies Communication
{
    shared formal Promise<Anything> readIntoBuffer(ByteBuffer buffer);
    shared formal Promise<Anything> writeFromBuffer(ByteBuffer buffer);

    ByteBuffer buffer4 = ByteBuffer.ofSize(4);

    shared actual Promise<Integer> readInt32()
    {
        buffer4.clear();
        readIntoBuffer(buffer4);
        buffer4.flip();
        return emptyPromise(buffer4.get().unsigned.leftLogicalShift(24)
            .or(buffer4.get().unsigned.leftLogicalShift(16))
            .or(buffer4.get().unsigned.leftLogicalShift(8))
            .or(buffer4.get().unsigned));
    }

    shared actual Promise<Anything> writeInt32(Integer val)
    {
        writeByte(val.rightLogicalShift(24).byte);
        writeByte(val.rightLogicalShift(16).byte);
        writeByte(val.rightLogicalShift(8).byte);
        writeByte(val.byte);
        return emptyPromise(null);
    }

    shared actual Promise<[Byte*]> readBytes()
    {
        variable value buffer = ByteBuffer.ofSize(0);
        return readInt32()
            .flatMap((length)
        {
            buffer = ByteBuffer.ofSize(length);
            return readIntoBuffer(buffer);
        })
        .flatMap((ignored)
        {
            buffer.flip();
            return emptyPromise(buffer.sequence());
        });
    }

    shared actual Promise<Anything> writeBytes({Byte*} bytes)
    {
        Array<Byte> dataToWrite =
                if (is Array<Byte> bytes)
                then bytes
                else Array<Byte>(bytes);

        writeInt32(dataToWrite.size);
        value buffer = ByteBuffer.ofArray(dataToWrite);
        writeFromBuffer(buffer);
        return emptyPromise(null);
    }

    shared actual Promise<String> readString()
    {
        return readBytes()
        .flatMap((bytes) => emptyPromise(utf8.decode(bytes)));
    }

    shared actual Promise<Anything> writeString(String val)
    {
        List<Byte> bytes = utf8.encode(val);
        writeBytes(bytes);
        return emptyPromise(null);
    }
}
