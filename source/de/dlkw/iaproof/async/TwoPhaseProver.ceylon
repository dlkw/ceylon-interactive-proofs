import de.dlkw.iaproof.async.io {
    Communication,
    Codec
}
import de.dlkw.iaproof.sync {
    ProgressIndicator
}
import ceylon.promise {
    Deferred,
    Promise
}

shared abstract class TwoPhaseProver<Phase1Result, Info, Challenge, Phase2Result>(codecPhase1, challengeCodec, codecPhase2)
        satisfies Prover
        given Phase1Result satisfies Object
        given Challenge satisfies Object
        given Phase2Result satisfies Object
{
    Codec<Phase1Result> codecPhase1;
    Codec<Challenge> challengeCodec;
    Codec<Phase2Result> codecPhase2;

    shared actual Promise<Anything> prove(Communication verifierIO, ProgressIndicator? progressIndicator)
    {
        log.debug("p reading repeats");
        return verifierIO.readInt32().flatMap((repeats)
        {
            log.debug("p read repeats: ``repeats``");
            if (exists progressIndicator) {
                progressIndicator.startSub(repeats, "verifying phases");
            }

            value promise = proveRemainingPhases(repeats, verifierIO, progressIndicator);

            if (exists progressIndicator) {
                progressIndicator.endSub();
            }

            return promise;
        });
    }

    Nothing loggingRethrow(Throwable throwable)
    {
        log.error("error:", throwable);
        throw throwable;
    }

    Promise<Anything> proveRemainingPhases(Integer remainingRepeats, Communication verifierIO, ProgressIndicator? progressIndicator)
    {
        assert (remainingRepeats > 0);

        log.debug("p still to go: ``remainingRepeats``");

        Promise<[Phase1Result, Info]> phase1Promise = performPhase1();
        log.debug("p returned");
        Promise<Anything> writePromise = phase1Promise.map(([commitment, p1Info])
        {
            log.debug("p performed calculations for phase 1");
            return nothing;
//            return codecPhase1.encodeAndWrite(commitment, verifierIO);
        }, loggingRethrow);

        // don't work...
//        Promise<Challenge> challengePromise = phase1Promise.and(writePromise).flatMap((ignored, [a, b])
        Promise<Challenge> challengePromise = writePromise.flatMap((ignored)
        {
            log.debug("p wrote commitment");// ``commitment``");
            return verifierIO.flush();
        })
        .flatMap((ignored)
        {
            log.debug("p flushed commitment");
            return challengeCodec.decode(verifierIO);
        });

        return phase1Promise.and(challengePromise).flatMap((challenge, [commitment, p1Info])
            {
                log.debug("p read challenge ``challenge``");
                return performPhase2(challenge, p1Info);
            })
            .flatMap((response)
            {
                return codecPhase2.encodeAndWrite(response, verifierIO);
            })
            .flatMap((ignored)
            {
                if (exists progressIndicator) {
                    progressIndicator.advance();
                }

                if (remainingRepeats == 1) {
                    value deferred = Deferred<Anything>();
                    deferred.fulfill(null);
                    return deferred.promise;
                }
                else {
                    return proveRemainingPhases(remainingRepeats - 1, verifierIO, progressIndicator);
                }
            });
    }

    shared formal Promise<[Phase1Result, Info]> performPhase1();
    shared formal Promise<Phase2Result> performPhase2(Challenge challenge, Info info);
}
