import de.dlkw.iaproof.async.io {
    Codec,
    InputHelper,
    OutputHelper
}
import ceylon.promise {
    Promise,
    Deferred
}
shared class NoData of noData
{
    shared new noData{}
}

shared object noDataCodec satisfies Codec<NoData>
{
    shared actual Promise<NoData> decode(InputHelper from)
    {
        value deferred = Deferred<NoData>();
        deferred.fulfill(NoData.noData);
        return deferred.promise;
    }

    shared actual Promise<Anything> encodeAndWrite(NoData data, OutputHelper to)
    {
        value deferred = Deferred<Anything>();
        deferred.fulfill(null);
        return deferred.promise;
    }
}
