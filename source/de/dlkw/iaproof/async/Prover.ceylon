import de.dlkw.iaproof.async.io {
    Communication
}
import de.dlkw.iaproof.sync {
    ProgressIndicator
}
import ceylon.logging {
    Logger,
    logger
}
import ceylon.promise {
    Promise
}

Logger log = logger(`package`);

shared interface Prover
{
    shared formal Promise<Anything> prove(Communication verifierIO, ProgressIndicator? progressIndicator = null);
}

shared interface Verifier
{
    shared formal Promise<Boolean> verify(Integer repeats, Communication provierIO, ProgressIndicator? progressIndicator = null);
}
