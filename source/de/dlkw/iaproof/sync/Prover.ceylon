import de.dlkw.iaproof.sync.io {
    InputHelper,
    OutputHelper,
    Communication
}
import ceylon.buffer {
    ByteBuffer
}
import ceylon.buffer.codec {
    StatelessCodec
}
import ceylon.buffer.readers {
    Reader
}
import ceylon.logging {
    logger,
    Logger
}

Logger log = logger(`package`);

shared interface Prover
{
    shared formal void prove(Communication verifierIO, ProgressIndicator? progressIndicator);
}

shared interface Verifier
{
    "Returns `true` if the knowledge of the prover is verified."
    shared formal Boolean verify(Integer repeats, Communication proverIO, ProgressIndicator? progressIndicator);
}

shared interface MissingWriter
{
    shared formal void write({Byte*} output);
}

shared class BufferingReader(Reader wrappedReader, Integer bufferSize)
{
    object iterator satisfies Iterator<Byte>
    {
        ByteBuffer buffer = ByteBuffer.ofSize(bufferSize);
        wrappedReader.read(buffer);
        buffer.flip();

        shared actual Byte|Finished next()
        {
            if (buffer.hasAvailable) {
                return buffer.get();
            }

            buffer.clear();
            wrappedReader.read(buffer);
            buffer.flip();

            if (buffer.hasAvailable) {
                return buffer.get();
            }
            return finished;
        }
    }
    object iterable satisfies Iterable<Byte>
    {
        shared actual Iterator<Byte> iterator() => object satisfies Iterator<Byte>
        {
            shared actual Byte|Finished next() => nothing;
        };
    }

    variable Boolean called = false;
    shared {Byte*} bytes
    {
        if (called) {
            throw Exception("may be iterated only once");
        }
        called = true;
        return iterable;
    }
}
