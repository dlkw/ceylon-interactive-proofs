import de.dlkw.iaproof.sync.io {
    Communication,
    Codec
}
import ceylon.buffer.codec {
    StatelessCodec
}
import ceylon.logging {
    Logger,
    logger
}


shared abstract class TwoPhaseProver<Phase1Result, Info, Challenge, Phase2Result>(codecPhase1, challengeCodec, codecPhase2)
    satisfies Prover
    given Phase1Result satisfies Object
    given Challenge satisfies Object
    given Phase2Result satisfies Object
{
    Codec<Phase1Result> codecPhase1;
    Codec<Challenge> challengeCodec;
    Codec<Phase2Result> codecPhase2;
//    value ioUtil = IOUtil(codecPhase1);

    shared actual void prove(Communication verifierIO, ProgressIndicator? progressIndicator)
    {
        log.debug("reading repeats");
        Integer repeats = verifierIO.readInt32();
        log.debug("read repeats: ``repeats``");
        if (exists progressIndicator) {
            progressIndicator.startSub(repeats, "proving phases");
        }

        for (i in 0:repeats) {
            log.debug("p iteration ``i``");

            value [commitment, p1Info] = performPhase1();
            codecPhase1.encodeAndWrite(commitment, verifierIO);
            log.debug("wrote commitment ``commitment``");
            verifierIO.flush();

            Challenge challenge = challengeCodec.decode(verifierIO);
            log.debug("read challenge ``challenge``");

            Phase2Result response = performPhase2(challenge, p1Info);
            codecPhase2.encodeAndWrite(response, verifierIO);
            log.debug("wrote phase2 ``response``");

            if (exists progressIndicator) {
                progressIndicator.advance();
            }
        }
        verifierIO.flush();

        if (exists progressIndicator) {
            progressIndicator.endSub();
        }
    }

    shared formal [Phase1Result, Info] performPhase1();
    shared formal Phase2Result performPhase2(Challenge challenge, Info info);
}

shared abstract class TwoPhaseVerifier<Phase1Result, Challenge, Phase2Result>(
    codecPhase1,
    codecChallenge,
    codecPhase2)

    satisfies Verifier
    given Phase1Result satisfies Object
    given Challenge satisfies Object
    given Phase2Result satisfies Object
{
    Codec<Phase1Result> codecPhase1;
    Codec<Challenge> codecChallenge;
    Codec<Phase2Result> codecPhase2;

    shared actual Boolean verify(Integer repeats, Communication proverIO, ProgressIndicator? progressIndicator)
    {
        log.debug("writing repeats");
        proverIO.writeInt32(repeats);
        proverIO.flush();
        log.debug("wrote repeats: ``repeats``");

        if (exists progressIndicator) {
            progressIndicator.startSub(repeats, "verifying phases");
        }

        for (i in 0:repeats) {
            log.debug("v iteration ``i``");

            Phase1Result commitment = codecPhase1.decode(proverIO);
            log.debug("read commitment ``commitment``");

            if (!verifyPhase1(commitment)) {
                return false;
            }

            Challenge challenge = createChallenge();
            codecChallenge.encodeAndWrite(challenge, proverIO);
            proverIO.flush();
            log.debug("wrote challenge ``challenge``");

            Phase2Result response = codecPhase2.decode(proverIO);
            log.debug("read phase2 ``response``");

            if (!verifyPhase2(response, commitment, challenge)) {
                return false;
            }

            if (exists progressIndicator) {
                progressIndicator.advance();
            }
        }

        if (exists progressIndicator) {
            progressIndicator.endSub();
        }

        return true;
    }

    "Returns `true` if the commitment passes phase 1 verification."
    shared formal Boolean verifyPhase1(Phase1Result commitment);
    shared formal Challenge createChallenge();

    "Returns `true` if the response passes phase 2 verification."
    shared formal Boolean verifyPhase2(Phase2Result response, Phase1Result commitment, Challenge challenge);
}
