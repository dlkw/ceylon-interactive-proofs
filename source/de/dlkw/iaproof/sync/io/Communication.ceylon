import ceylon.buffer {
    ByteBuffer
}
import ceylon.buffer.charset {
    utf8
}

shared interface InputHelper
{
    shared formal Byte? readByte();
    shared formal Integer readInt32();
    shared formal [Byte*] readBytes();
    shared formal String readString();
}

shared interface OutputHelper
{
    shared formal void writeByte(Byte byte);
    shared formal void writeInt32(Integer val);
    shared formal void writeBytes({Byte*} bytes);
    shared formal void writeString(String val);

    shared formal void flush();
}

shared interface Communication
        satisfies InputHelper & OutputHelper
{
}

shared abstract class CommunicationCore()
        satisfies Communication
{
    shared formal void readIntoBuffer(ByteBuffer buffer);
    shared formal void writeFromBuffer(ByteBuffer buffer);

    ByteBuffer buffer4 = ByteBuffer.ofSize(4);

    shared actual Integer readInt32()
    {
        buffer4.clear();
        readIntoBuffer(buffer4);
        buffer4.flip();
        return buffer4.get().unsigned.leftLogicalShift(24)
            .or(buffer4.get().unsigned.leftLogicalShift(16))
            .or(buffer4.get().unsigned.leftLogicalShift(8))
            .or(buffer4.get().unsigned);
    }

    shared actual void writeInt32(Integer val)
    {
        writeByte(val.rightLogicalShift(24).byte);
        writeByte(val.rightLogicalShift(16).byte);
        writeByte(val.rightLogicalShift(8).byte);
        writeByte(val.byte);
    }

    shared actual [Byte*] readBytes()
    {
        value length = readInt32();
        value buffer = ByteBuffer.ofSize(length);
        readIntoBuffer(buffer);
        buffer.flip();
        return buffer.sequence();
    }

    shared actual void writeBytes({Byte*} bytes)
    {
        Array<Byte> dataToWrite =
                if (is Array<Byte> bytes)
                then bytes
                else Array<Byte>(bytes);

        writeInt32(dataToWrite.size);
        value buffer = ByteBuffer.ofArray(dataToWrite);
        writeFromBuffer(buffer);
    }

    shared actual String readString()
    {
        Byte[] bytes = readBytes();
        return utf8.decode(bytes);
    }

    shared actual void writeString(String val)
    {
        List<Byte> bytes = utf8.encode(val);
        writeBytes(bytes);
    }
}
