shared interface Codec<Data>
{
    shared formal void encodeAndWrite(Data data, OutputHelper to);
    shared formal Data decode(InputHelper readFrom);
}

shared object booleanCodec satisfies Codec<Boolean>
{
    shared actual Boolean decode(InputHelper from)
    {
        assert (exists byte = from.readByte());
        return !byte.zero;
    }

    shared actual void encodeAndWrite(Boolean data, OutputHelper to)
    {
        to.writeByte(data then 1.byte else 0.byte);
    }
}

shared object voidCodec satisfies Codec<Nothing>
{
    shared actual Nothing decode(InputHelper from) => nothing;
    shared actual void encodeAndWrite(Nothing data, OutputHelper to) {}
}