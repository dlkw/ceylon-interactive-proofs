shared interface ProgressIndicator
{
    shared formal void startSub(Integer total, String message);
    shared formal void advance();
    shared formal void endSub();
}

class LoggingPI(String name) satisfies ProgressIndicator
{
    variable Integer level = 0;

    shared actual void advance()
    {
        log.info("``name``: advance");
    }

    shared actual void endSub()
    {
        log.info("``name``: end level ``level--`` sub");
    }

    shared actual void startSub(Integer total, String message)
    {
        log.info("``name``: start level ``++level`` sub ``message`` with ``total`` steps");
    }
}
