import de.dlkw.iaproof.sync.io {
    Codec,
    InputHelper,
    OutputHelper
}

shared class NoData of noData
{
    shared new noData{}
}

shared object noDataCodec satisfies Codec<NoData>
{
    shared actual NoData decode(InputHelper from) => NoData.noData;

    shared actual void encodeAndWrite(NoData data, OutputHelper to) {}
}
