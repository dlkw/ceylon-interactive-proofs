import de.dlkw.iaproof.sync.io {
    Codec,
    booleanCodec
}
import ceylon.random {
    Random
}

shared abstract class CoinFlipProver<Phase1Result, Info, Phase2Result>(
    codecPhase1, codecPhase2, random)

    extends TwoPhaseProver<Phase1Result, Info, Boolean, Phase2Result>(
        codecPhase1, booleanCodec, codecPhase2)

    given Phase1Result satisfies Object
    given Phase2Result satisfies Object
{
    Codec<Phase1Result> codecPhase1;
    Codec<Phase2Result> codecPhase2;
    shared Random random;

    shared actual Phase2Result performPhase2(Boolean coin, Info info)
    {
        log.debug("show ``coin then "U" else "V"``");
        return coin then performPhase2U(info) else performPhase2V(info);
    }

    shared formal Phase2Result performPhase2U(Info info);
    shared formal Phase2Result performPhase2V(Info info);
}

shared abstract class CoinFlipVerifier<Phase1Result, Phase2Result>(
    codecPhase1, codecPhase2, random)

    extends TwoPhaseVerifier<Phase1Result, Boolean, Phase2Result>(
        codecPhase1, booleanCodec, codecPhase2)

    given Phase1Result satisfies Object
    given Phase2Result satisfies Object
{
    Codec<Phase1Result> codecPhase1;
    Codec<Phase2Result> codecPhase2;
    Random random;

    shared actual Boolean createChallenge()
    {
        return random.nextBoolean();
    }

    shared actual Boolean verifyPhase2(Phase2Result response, Phase1Result reference, Boolean coin)
    {
        log.debug("verify ``coin then "U" else "V"``");
        return coin then verifyPhase2U(response, reference) else verifyPhase2V(response, reference);
    }

    shared formal Boolean verifyPhase2U(Phase2Result response, Phase1Result reference);
    shared formal Boolean verifyPhase2V(Phase2Result response, Phase1Result reference);
}