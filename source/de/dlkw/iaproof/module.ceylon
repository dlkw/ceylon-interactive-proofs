"Performing interactive proofs."

module de.dlkw.iaproof "0.0.1" {
    shared import ceylon.buffer "1.3.2";
    shared import ceylon.promise "1.3.2";
    shared import ceylon.random "1.3.2";

    import ceylon.logging "1.3.2";
}
