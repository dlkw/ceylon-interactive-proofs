import de.dlkw.iaproof.sync.io {
    CommunicationCore
}
import de.dlkw.iaproof.async.io {
    CommunicationCoreAX = CommunicationCore,
    emptyPromise
}
import test.de.dlkw.iaproof.cavedoor {
    CaveDoorProver,
    CaveDoorVerifier,
    CaveDoorProverAX,
    CaveDoorVerifierAX
}
import ceylon.logging {
    Logger,
    logger,
    addLogWriter,
    writeSimpleLog,
    defaultPriority,
    debug,
    info,
    Priority,
    Category
}
import java.lang {
    Runnable, Thread,
    ByteArray
}
import java.util.concurrent {
    Semaphore
}
import java.io {
    InputStream, OutputStream, PipedInputStream, PipedOutputStream
}
import ceylon.buffer {
    ByteBuffer
}
import ceylon.interop.java {
    createJavaByteArray
}
import ceylon.promise {
    Deferred,
    Promise
}
import ceylon.locale {
    systemLocale
}
import ceylon.time {
    Instant,
    now
}
import de.dlkw.iaproof.async {
    defer
}


shared void run()
{
    addLogWriter{    void log(Priority p, Category c, String m, Throwable? t) {
        value print
                = p <= info
        then process.write
        else process.writeError;
        value instant = now();
        value formats = systemLocale.formats;
        value time =
                formats.mediumFormatTime(instant.time());
        print("[``time``] ``p.string``: ``c`` ``m``");
        print(operatingSystem.newline);
        if (exists t) {
            printStackTrace(t, print);
        }
    }};
    defaultPriority = debug;

    runDoor();
}

native void runDoor();

native("js") void runDoor()
{
    defer = (Anything() p)
    {
        p();
/*
        dynamic {
            setImmediate(p);
        }
        */
    };

    CommunicationCoreAX x;
    CommunicationCoreAX y;
    dynamic {
        dynamic pToV = require("stream").PassThrough();
        dynamic vToP = require("stream").PassThrough();
        x = JsIo(pToV, vToP);
        y = JsIo(vToP, pToV);
    }

    variable Integer done = 0;

    CaveDoorVerifierAX().verify(20, x).map((x)
    {
        done += 1;
    }, (t)
    {
        logger(`package`).error("error on verify", t);
        done += 1;
    });
    CaveDoorProverAX().prove(y).map((x)
    {
        done += 1;
    }, (t)
    {
        logger(`package`).error("error on prove", t);
        done += 1;
    });

    /*
    value f = x.readByte();
    value b = f.map<Byte>((bb)
    {
        print("outer ``bb``");
        return bb;
    });
    x.readByte().map<Byte>((bb)
    {
        print("outer ``bb``");
        return bb;
    });
    */


    /*
    value ba1 = ByteBuffer.ofSize(2);
    value ba2 = ByteBuffer.ofSize(2);
    value ba3 = ByteBuffer.ofSize(2);
    value g = x.readIntoBuffer(ba1);
    value c = g.flatMap<Anything>((bb)
    {
        print("outer ``ba1.array``");
        return x.readIntoBuffer(ba2);
    }).flatMap<Anything>((bb)
    {
        print("outer ``ba2.array``");
        return x.readIntoBuffer(ba3);
    }).map((bb)
    {
        print("outer ``ba3.array``");
        done = true;
        return bb;
    });

    value bb1 = ByteBuffer.ofArray(Array({55.byte, 56.byte, 57.byte}));
    y.writeFromBuffer(bb1);

    value bb2 = ByteBuffer.ofArray(Array({58.byte, 59.byte, 60.byte}));
    y.writeFromBuffer(bb2);
    */


    void waitForDone()
    {
        if (done < 2) {
            dynamic {
                setImmediate(() => waitForDone());
            }
        }
    }

    waitForDone();
}

native("js") class JsIo(dynamic inStream, dynamic outStream)
    extends CommunicationCoreAX()
{
    shared actual Promise<Byte> readByte()
    {
        value deferred = Deferred<Byte>();
        dynamic {
            inStream.on("readable", ()
            {
                    assert (exists chunk = inStream.read(1));
                dynamic v = chunk[0];
                assert (is Integer v);
                print("inner ``v``");
                    deferred.fulfill(Byte(v));
            });
        }
        return deferred.promise;
    }

    shared actual Promise<Anything> writeByte(Byte val)
    {
        dynamic {
            dynamic buf = Buffer(dynamic [val]);
            outStream.write(buf);//callback fulfilling deferred?
        }
        return emptyPromise(null);
    }

    shared actual Promise<Anything> flush()
    {
        return emptyPromise(null);
    }

    shared actual Promise<Anything> readIntoBuffer(ByteBuffer cBuffer)
    {
        void transferToByteBuffer(dynamic nodeBuf, ByteBuffer buffer)
        {
            Integer[] array;
            dynamic {
                array = [for (i in 0:nodeBuf.length) if (is Integer el = nodeBuf.get(i)) el];
            }
            array.map((el) => el.byte)
                .each(cBuffer.put);
        }

        value deferred = Deferred<Anything>();

        void cp()
        {
            dynamic {
                print("r: need ``cBuffer.available``");
                dynamic nodeBuffer = inStream.read(cBuffer.available);
                if (is Null nodeBuffer) {
                    print("r: not enough!");
                    dynamic buu = inStream.read();
                    if (is Null buu) {
                        print("r: nothing at all");
                        inStream.once("readable", ()
                        {
                            print("r: got more data");
                            cp();
                        });
                    }
                    else {
                        if (buu.length>cBuffer.available) {
                            print("r: too much!");
                            inStream.unshift(buu.slice(cBuffer.available));
                        }
                        transferToByteBuffer(buu.slice(0, cBuffer.available), cBuffer);
                        cp();
                    }
                } else {
                    print("r: got all");
                    transferToByteBuffer(nodeBuffer, cBuffer);
                    deferred.fulfill(null);
                }
            }
        }

        cp();
        return deferred.promise;
    }

    shared actual Promise<Anything> writeFromBuffer(ByteBuffer buffer)
    {
        value deferred = Deferred<Anything>();
        dynamic {
            dynamic buf = Buffer(dynamic [*buffer]);
            print("w: writing ``buf.length``");
            outStream.write(buf, (dynamic ignored)
            {
                deferred.fulfill(null);
            });
        }
        return deferred.promise;
    }
}

native("jvm") void runDoor()
{
    // the prover and verifier are started in separate threads
    // so no need to defer, calculations can be performed immediately
    defer = (Anything() p) => p();

    Logger log = logger(`package`);

    log.debug("starting");
    log.info("starting");

    PipedInputStream pipe1In = PipedInputStream();
    PipedOutputStream pipe2Out = PipedOutputStream(pipe1In);
    PipedInputStream pipe2In = PipedInputStream();
    PipedOutputStream pipe1Out = PipedOutputStream(pipe2In);

    Thread t1 = Thread((){
        log.debug("t1");
        CaveDoorProver().prove(JioIn(pipe1In, pipe1Out), null);
    });

    Thread t2 = Thread((){
        log.debug("t2");
        CaveDoorVerifier().verify(200, JioIn(pipe2In, pipe2Out), null);
    });

    Thread t3 = Thread((){
        log.debug("t3");
        Semaphore sem = Semaphore(1, false);
        sem.acquire();
        CaveDoorProverAX().prove(JioAX(JioIn(pipe1In, pipe1Out)), null)
        .map((s)
        {
            log.debug("p releasing");
            sem.release();
            log.debug("p released");
        }, (t)
        {
            log.error("error on proving", t);
            log.debug("p releasing");
            sem.release();
            log.debug("p released");
        });
        log.debug("p acq");
        sem.acquire();
        log.debug("p acqd");
    });

    Thread t4 = Thread((){
        log.debug("t4");
        Semaphore sem = Semaphore(1, false);
        sem.acquire();
        CaveDoorVerifierAX().verify(201, JioAX(JioIn(pipe2In, pipe2Out)), null)
            .map((s)
        {
            log.debug("v releasing");
            sem.release();
            log.debug("v released");
        }, (t)
        {
            log.error("error on verifying", t);
            log.debug("v releasing");
            sem.release();
            log.debug("v released");
        });

        log.debug("v acq");
        sem.acquire();
        log.debug("v acqd");
    });

    value startTime = now();
    t3.start();
    t4.start();
    t3.join();
    t4.join();
    value endTime = now();
    log.info("ok. ``startTime.durationTo(endTime)``");
}

native("jvm") class JioIn(InputStream inputStream, OutputStream outputStream) extends CommunicationCore()
{
    shared actual Byte? readByte()
    {
        Integer byte = inputStream.read();
        return if (byte == -1) then null else byte.byte;
    }

    shared actual void writeByte(Byte byte)
    {
        outputStream.write(byte.unsigned);
    }

    shared actual void readIntoBuffer(ByteBuffer buffer)
    {
        ByteArray ba = ByteArray.from(buffer.array);
        variable Integer off = 0;
        variable Integer length = buffer.available;
        while (length > 0) {
            Integer count = inputStream.read(ba, buffer.position + off, length);
            off += count;
            length -= count;
        }
        buffer.position += off;
    }

    shared actual void writeFromBuffer(ByteBuffer buffer)
    {
        ByteArray ba = ByteArray.from(buffer.array);
        outputStream.write(ba, buffer.position, buffer.available);
    }

    shared actual void flush()
    {
        outputStream.flush();
    }
}

native("jvm") class JioAX(JioIn jioIn) extends CommunicationCoreAX()
{
    shared actual Promise<Anything> flush()
    {
        return emptyPromise(jioIn.flush());
    }

    shared actual Promise<Byte?> readByte()
    {
        return emptyPromise(jioIn.readByte());
    }

    shared actual Promise<Anything> readIntoBuffer(ByteBuffer buffer)
    {
        return emptyPromise(jioIn.readIntoBuffer(buffer));
    }

    shared actual Promise<Anything> writeByte(Byte val)
    {
        return emptyPromise(jioIn.writeByte(val));
    }

    shared actual Promise<Anything> writeFromBuffer(ByteBuffer buffer)
    {
        return emptyPromise(jioIn.writeFromBuffer(buffer));
    }
}
