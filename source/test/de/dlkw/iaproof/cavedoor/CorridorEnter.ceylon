import de.dlkw.iaproof.sync.io {
    InputHelper,
    OutputHelper,
    Codec
}

shared class CorridorEnter of enterLeft | enterRight
{
    shared new enterLeft{}
    shared new enterRight{}
}

class CorridorEnterCodec() satisfies Codec<CorridorEnter>
{
    shared actual CorridorEnter decode(InputHelper from)
    {
        String s = from.readString();
        switch (s)
        case ("enterLeft") {
            return CorridorEnter.enterLeft;
        }
        case ("enterRight") {
            return CorridorEnter.enterRight;
        }
        else {
            throw AssertionError(s);
        }
    }

    shared actual void encodeAndWrite(CorridorEnter data, OutputHelper to)
    {
        to.writeString(data.string);
    }
}