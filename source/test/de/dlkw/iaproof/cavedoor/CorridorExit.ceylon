import de.dlkw.iaproof.async.io {
    InputHelperAX = InputHelper,
    OutputHelperAX = OutputHelper,
    CodecAX = Codec
}
import de.dlkw.iaproof.sync.io {
    InputHelper,
    OutputHelper,
    Codec
}
import ceylon.promise {
    Deferred,
    Promise
}

shared class CorridorExit of exitLeft | exitRight
{
    shared new exitLeft{}
    shared new exitRight{}
}

class CorridorExitCodec() satisfies Codec<CorridorExit>
{
    shared actual CorridorExit decode(InputHelper from)
    {
        String s = from.readString();
        switch (s)
        case ("exitLeft") {
            return CorridorExit.exitLeft;
        }
        case ("exitRight") {
            return CorridorExit.exitRight;
        }
        else {
            throw AssertionError(s);
        }
    }

    shared actual void encodeAndWrite(CorridorExit data, OutputHelper to)
    {
        switch (data)
        case (CorridorExit.exitLeft) {
            to.writeString("exitLeft");
        }
        case (CorridorExit.exitRight) {
            to.writeString("exitRight");
        }
    }
}

class CorridorExitCodecAX() satisfies CodecAX<CorridorExit>
{
    shared actual Promise<CorridorExit> decode(InputHelperAX from)
    {
        return from.readString()
        .flatMap((s)
        {
            value deferred = Deferred<CorridorExit>();
            switch (s)
            case ("exitLeft") {
                deferred.fulfill(CorridorExit.exitLeft);
            }
            case ("exitRight") {
                deferred.fulfill(CorridorExit.exitRight);
            }
            else {
                deferred.reject(AssertionError(s));
            }
            return deferred.promise;
        });
    }

    shared actual Promise<Anything> encodeAndWrite(CorridorExit data, OutputHelperAX to)
    {
        switch (data)
        case (CorridorExit.exitLeft) {
            return to.writeString("exitLeft");
        }
        case (CorridorExit.exitRight) {
            return to.writeString("exitRight");
        }
    }
}
