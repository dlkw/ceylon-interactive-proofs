class Door()
{
    shared CorridorExit goThroughDoor(CorridorEnter enter, String key)
    {
        if (key == "matchingKey") {
            switch (enter)
            case (CorridorEnter.enterLeft) {
                return CorridorExit.exitRight;
            }
            case (CorridorEnter.enterRight) {
                return CorridorExit.exitLeft;
            }
        }
        else {
            switch (enter)
            case (CorridorEnter.enterLeft) {
                return CorridorExit.exitLeft;
            }
            case (CorridorEnter.enterRight) {
                return CorridorExit.exitRight;
            }
        }
    }

    shared CorridorExit turnAroundAtDoor(CorridorEnter enter) {
        switch (enter)
        case (CorridorEnter.enterLeft) {
            return CorridorExit.exitLeft;
        }
        case (CorridorEnter.enterRight) {
            return CorridorExit.exitRight;
        }
    }
}