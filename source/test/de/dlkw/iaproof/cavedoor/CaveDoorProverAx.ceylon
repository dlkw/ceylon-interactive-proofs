import de.dlkw.iaproof.async {
    CoinFlipProver,
    CoinFlipVerifier,
    NoData,
    noDataCodec,
    doDefer
}
import de.dlkw.iaproof.async.io {
    emptyPromise
}
import ceylon.random {
    DefaultRandom
}
import ceylon.promise {
    Deferred,
    Promise
}

shared class CaveDoorProverAX()
extends CoinFlipProver<NoData, CorridorEnter, CorridorExit>(
    noDataCodec, CorridorExitCodecAX(), DefaultRandom(0))
{
    value rnd2 = DefaultRandom(0);
    Door door = Door();
    String key = "matchingKey";

    shared actual Promise<[NoData, CorridorEnter]> performPhase1()
    {
        value deferred = Deferred<[NoData, CorridorEnter]>();

        return doDefer(()
        {
            print("p: performing phase 1 calc");
            Boolean nx = rnd2.nextBoolean();
            CorridorEnter enter = nx then CorridorEnter.enterLeft else CorridorEnter.enterRight;
            print("p: done phase 1 calc");
            return [NoData.noData, enter];
        });
    }

    shared actual Promise<CorridorExit> performPhase2U(CorridorEnter enter)
    {
        // exit from left corridor

        value deferred = Deferred<CorridorExit>();
        switch (enter)
        case (CorridorEnter.enterLeft) {
            deferred.fulfill(door.turnAroundAtDoor(enter));
        }
        case (CorridorEnter.enterRight) {
            deferred.fulfill(door.goThroughDoor(enter, key));
        }
        return deferred.promise;
    }

    shared actual Promise<CorridorExit> performPhase2V(CorridorEnter enter)
    {
        // exit from right corridor

        value deferred = Deferred<CorridorExit>();
        switch (enter)
        case (CorridorEnter.enterLeft) {
            deferred.fulfill(door.goThroughDoor(enter, key));
        }
        case (CorridorEnter.enterRight) {
            deferred.fulfill(door.turnAroundAtDoor(enter));
        }
        return deferred.promise;
    }
}

shared class CaveDoorVerifierAX()
extends CoinFlipVerifier<NoData, CorridorExit>(noDataCodec, CorridorExitCodecAX(), DefaultRandom())
{
    shared actual Promise<Boolean> verifyPhase1(NoData ignore)
    {
        value deferred = Deferred<Boolean>();
        deferred.fulfill(true);
        return deferred.promise;
    }

    shared actual Promise<Boolean> verifyPhase2U(CorridorExit response, NoData reference)
    {
        value deferred = Deferred<Boolean>();
        deferred.fulfill(response == CorridorExit.exitLeft);
        return deferred.promise;
    }

    shared actual Promise<Boolean> verifyPhase2V(CorridorExit response, NoData reference)
    {
        value deferred = Deferred<Boolean>();
        deferred.fulfill(response == CorridorExit.exitRight);
        return deferred.promise;
    }
}
