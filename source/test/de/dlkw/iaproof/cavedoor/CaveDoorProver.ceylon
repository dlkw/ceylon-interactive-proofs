import de.dlkw.iaproof.sync {
    CoinFlipProver,
    NoData,
    noDataCodec,
    CoinFlipVerifier
}
import ceylon.random {
    DefaultRandom
}

shared class CaveDoorProver()
extends CoinFlipProver<NoData, CorridorEnter, CorridorExit>(
    noDataCodec, CorridorExitCodec(), DefaultRandom(0))
{
    Door door = Door();
    String key = "matchingKey";

    shared actual [NoData, CorridorEnter] performPhase1()
    {
        CorridorEnter enter = random.nextBoolean() then CorridorEnter.enterLeft else CorridorEnter.enterRight;
        return [NoData.noData, enter];
    }

    shared actual CorridorExit performPhase2U(CorridorEnter enter)
    {
        // exit from left corridor

        switch (enter)
        case (CorridorEnter.enterLeft) {
            return door.turnAroundAtDoor(enter);
        }
        case (CorridorEnter.enterRight) {
            return door.goThroughDoor(enter, key);
        }
    }

    shared actual CorridorExit performPhase2V(CorridorEnter enter)
    {
        // exit from right corridor

        switch (enter)
        case (CorridorEnter.enterLeft) {
            return door.goThroughDoor(enter, key);
        }
        case (CorridorEnter.enterRight) {
            return door.turnAroundAtDoor(enter);
        }
    }
}

shared class CaveDoorVerifier()
extends CoinFlipVerifier<NoData, CorridorExit>(noDataCodec, CorridorExitCodec(), DefaultRandom())
{
    shared actual Boolean verifyPhase1(NoData ignore) => true;

    shared actual Boolean verifyPhase2U(CorridorExit response, NoData reference) => response == CorridorExit.exitLeft;

    shared actual Boolean verifyPhase2V(CorridorExit response, NoData reference) => response == CorridorExit.exitRight;

}