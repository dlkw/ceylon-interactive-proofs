"Tests for module `de.dlkw.iaproof`."

module test.de.dlkw.iaproof "0.0.1" {
    shared import de.dlkw.iaproof "0.0.1";

    shared import ceylon.promise "1.3.2";

    import ceylon.locale "1.3.2";
    import ceylon.logging "1.3.2";
    import ceylon.random "1.3.2";
    import ceylon.time "1.3.2";

    native("jvm") import java.base "8";
    native("jvm") import ceylon.interop.java "1.3.2";
}
