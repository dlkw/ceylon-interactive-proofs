import ceylon.promise {
    Deferred,
    Promise
}
import java.util.concurrent {
    Semaphore
}

native shared void promtest()
{
    print("not implemented for this backend");
}

native("jvm") shared void promtest()
{
    Promise<Integer> calc1()
    {
        value deferred = Deferred<Integer>();
        deferred.fulfill(4);
        return deferred.promise;
    }

    Promise<String> calc2()
    {
        value deferred = Deferred<String>();
        deferred.fulfill("pp");
        return deferred.promise;
    }

    String calc2b() => "qq";

    Promise<Float> calc3()
    {
        value deferred = Deferred<Float>();
        deferred.fulfill(2.3);
        return deferred.promise;
    }

    Promise<Integer> p1 = calc1();

    Promise<String> p2 = p1.flatMap((i)
    {
        print(i);
        return calc2();
    });

    value sem = Semaphore(1);
    sem.acquire();
    Promise<Float> p3 = p2.flatMap((s)
    {
        print(s);
        return calc3();
    });

    p1.and(p2).and(p3).map((e3, e2, e1)
    {
        print("``e1`` - ``e2`` - ``e3``");
        print(88);
        sem.release();
        return 88;
    });

    //print("a");
    sem.acquire();
    //print("b");
}
